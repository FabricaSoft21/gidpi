﻿//-------------------------------INICIA TIPS ÁRBOL DE PROBLEMAS-------------------------------------

function tipArbolProblema() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "La lógica del árbol de problemas se realiza de abajo hacia arriba.",

       {
           type: "success", align: "left", verticalAlign: "middle",
           delay: 10000, animation: true, animationType: "scale", close: true

       });

}

function tipArbolProblemaDos() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Los cuadros del árbol de problemas son editables, por lo que puedes acomodar a gusto su redacción.",

       {
           type: "success", align: "left", verticalAlign: "middle",
           delay: 10000, animation: true, animationType: "scale", close: true

       });

}

function tipArbolProblemaTres() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Si realizaste la matriz de Vester, la cantidad de causas y efectos de tu árbol de problemas no tienen que ser la misma cantidad de situaciones que redactaste en tu matriz de Vester, por lo que puedes añadir y eliminar causas y efectos para que quede más claro tu árbol de problemas.",

       {
           type: "success", align: "left", verticalAlign: "middle",
           delay: 10000, animation: true, animationType: "scale", close: true

       });

}

//-------------------------------FINALIZA TIPS ÁRBOL DE PROBLEMAS-------------------------------------




//------------------------INICIA TIPS ÁRBOL DE OBJETIVOS--------------------------------------------

//------------------------INICIA TIPS ÁRBOL DE OBJETIVOS TAB 1--------------------------------------

function tipArbolObjetivos() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "En lo posible, cambiar sólo la palabra negativa a positiva, para no perder el enfoque de la situación problemática.",

       {
           type: "success", align: "left", verticalAlign: "bottom",
           delay: 10000, animation: true, animationType: "scale", close: true

       });

}

//------------------------FINALIZA TIPS ÁRBOL DE OBJETIVOS TAB 1--------------------------------------


//-----------------------INICIA TIPS OBJETIVOS TAB 2-------------------------------------------------------

function tipObjetivos() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Verbo en infinitivo + sujeto + condición + criterio",

       { 
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });

}

function tipObjetivosDos() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Ningún objetivo debe contener dos o más verbos en infinitivo.",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}


function tipObjetivosTres() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "El objetivo general y los objetivos específicos deben tener los siguientes requisitos según la Metodología SMART ideada por George T. Doran. <b>S:</b> ESPECÍFICO <b>M:</b> MEDIBLE <b>A:</b> ALCANZABLE <b>R:</b> RELEVANTE <b>T:</b> CON UN TIEMPO DETERMINADO.",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}

function tipObjetivosCuatro() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Debe tener claridad de que cada uno de los resultados esté asociado a un objetivo específico del proyecto y que cada producto esté asociado a un resultado.",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}

function tipObjetivosCinco() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Las causas directas en el árbol de problemas al pasarlas en positivo se convierten en los objetivos específicos.",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}


//-----------------------FINALIZA TIPS OBJETIVOS TAB 2-------------------------------------------------------




//-----------------------INICIA TIPS RESULTADOS TAB 3-------------------------------------------------------

function tipResultados() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Son intangibles los resultados se asemejan a los medios indirectos con la siguiente condición: <b>Nombre o sustantivo + verbo conjugado en participio pasivo (ado, edo, ido, to, so,cho) + un gerundio(sirven para especificar las características o cualidades esperadas de los resultados)</b>",

       {
           type: "success", align: "left", verticalAlign: "top",
           delay: 10000, animation: true, animationType: "scale", close: true

       });

}


function tipResultadosDos() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "<b>¿cómo se va a medir los resultados?</b></br>Se debe redactar de forma que se entienda: <br>-Cómo se va a medir</br>-Qué indicador va a utilizar y que se va a medir.<br> <b>Ejemplo:</b> Revisión del número de unidades de rejillas plásticas instaladas.",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}

function tipResultadosTres() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "<b>¿Qué herramienta va a utilizar para medir los resultados?</b></br> <b>Ejemplo:</b> Lista de chequeo, observación directa con evidencia fotográfica.",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}

function tipResultadosCuatro() {
    $.notify("<h5 style='text-align:center'><b>Para recordar...</b></h5>" + "Los productos son tangibles llevan la siguiente condición: (cantidad esperada + nombre o sustantivo +  un verbo conjugado en participio pasivo(ado, edo, ido, to, so, cho) + un gerundio, que sirvan para especificar las características o cualidades esperadas de los resultados.)",

        {
            type: "success", align: "left", verticalAlign: "top",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}


//-----------------------FINALIZA TIPS RESULTADOS TAB 3-------------------------------------------------------


//------------------------FINALIZA TIPS ÁRBOL DE OBJETIVOS--------------------------------------------








//------------------------INICIA TIPS MATRIZ DE INVOLUCRADOS--------------------------------------------


function usage() {
    $.notify("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam imperdiet arcu est,eu tincidunt tellus scelerisque in. Donec elementum commodo nunc. Nulla gravida pretium ante, nec vulputate turpis luctus ut. Sed ligula arcu, tincidunt eu imperdiet non, iaculis ac mauris. Praesent porttitor dapibus magna faucibus aliquet. Nunc commodo elementum ante non mollis. Sed viverra elit quis placerat ultricies. Cras et urna tristique, ornare nulla vel, placerat est. ",

        {
            type: "success", align: "left", verticalAlign: "bottom",
            delay: 10000, animation: true, animationType: "scale", close: true

        });
}
//------------------------FINALIZA TIPS MATRIZ DE INVOLUCRADOS--------------------------------------------